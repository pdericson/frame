defmodule Frame.AlbumTest do
  use ExUnit.Case

  test "path" do
    {:ok, _} = Frame.Album.path()
  end

  test "next" do
    {:ok, path1} = Frame.Album.next()
    {:ok, path2} = Frame.Album.next()
    {:ok, path3} = Frame.Album.next()

    assert path1 != path2
    assert path1 == path3

    {:ok, path4} = Frame.Album.path()

    assert path1 == path4 or path2 == path4
  end
end

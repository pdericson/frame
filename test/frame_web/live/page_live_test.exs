defmodule FrameWeb.PageLiveTest do
  use FrameWeb.ConnCase

  import Phoenix.LiveViewTest

  test "disconnected and connected render", %{conn: conn} do
    {:ok, page_live, disconnected_html} = live(conn, "/")
    assert disconnected_html =~ "<img"
    assert render(page_live) =~ "<img"
  end
end

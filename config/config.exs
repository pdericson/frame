# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :frame, FrameWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "UgTk9Fy7RjtsSy2nXLUFnK/4lia2PTqitdFW0LTyZN8/+atpzA4B8wg1md6zl/fU",
  render_errors: [view: FrameWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Frame.PubSub,
  live_view: [signing_salt: "nwIRNU0A"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Phoenix Swagger
config :frame, :phoenix_swagger,
  swagger_files: %{
    "priv/static/swagger.json" => [
      router: FrameWeb.Router
    ]
  }

config :phoenix_swagger, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

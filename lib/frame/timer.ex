defmodule Frame.Timer do
  @moduledoc """
  This is the Timer module.
  """

  use Task

  def start_link(time) do
    Task.start_link(__MODULE__, :loop, [time])
  end

  def loop(seconds) do
    Process.sleep(seconds * 1000)

    {:ok, _} = Frame.Album.next()

    loop(seconds)
  end
end

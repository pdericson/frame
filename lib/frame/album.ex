defmodule Frame.Album do
  @moduledoc """
  This is the Album module.
  """

  use GenServer

  # Client

  def start_link(root) do
    GenServer.start_link(__MODULE__, root, name: __MODULE__)
  end

  @doc """
  Returns the current photo.
  """
  def path() do
    GenServer.call(__MODULE__, :path)
  end

  @doc """
  Returns the prev photo.
  """
  def prev() do
    GenServer.call(__MODULE__, :prev)
  end

  @doc """
  Returns the next photo.
  """
  def next() do
    GenServer.call(__MODULE__, :next)
  end

  @doc """
  Zoom.
  """
  def zoom() do
    GenServer.call(__MODULE__, :zoom)
  end

  @doc """
  Zoom out.
  """
  def zoom_out() do
    GenServer.call(__MODULE__, :zoom_out)
  end

  # Server (callbacks)

  def init(root) do
    paths = FileExt.ls_r(root)

    paths =
      paths
      |> Enum.filter(fn x ->
        Enum.any?(
          Enum.map([".jpg", ".jpeg", ".png"], fn y ->
            String.downcase(Path.extname(x)) == String.downcase(y)
          end)
        )
      end)

    paths = paths |> Enum.sort()

    path = Enum.random(paths)

    :dets.open_file(:frame_table, [{:file, 'frame_table'}])

    case :dets.lookup(:frame_table, path) do
      [{_, true}] ->
        update_bg(path, true)

      _ ->
        update_bg(path)
    end

    {:ok, %{root: root, paths: paths, path: path}}
  end

  def handle_call(:path, _from, %{path: path} = state) do
    {:reply, {:ok, path}, state}
  end

  def handle_call(:prev, _from, %{paths: paths, path: path} = state) do
    index =
      cond do
        Enum.find_index(paths, fn x -> x == path end) == 0 ->
          Enum.count(paths) - 1

        true ->
          Enum.find_index(paths, fn x -> x == path end) - 1
      end

    prev = Enum.at(paths, index)

    case :dets.lookup(:frame_table, prev) do
      [{_, true}] ->
        update_bg(prev, true)

      _ ->
        update_bg(prev)
    end

    state = Map.put(state, :path, prev)

    {:reply, {:ok, prev}, state}
  end

  def handle_call(:next, _from, %{paths: paths, path: path} = state) do
    index =
      cond do
        Enum.find_index(paths, fn x -> x == path end) + 1 == Enum.count(paths) ->
          0

        true ->
          Enum.find_index(paths, fn x -> x == path end) + 1
      end

    next = Enum.at(paths, index)

    case :dets.lookup(:frame_table, next) do
      [{_, true}] ->
        update_bg(next, true)

      _ ->
        update_bg(next)
    end

    state = Map.put(state, :path, next)

    {:reply, {:ok, next}, state}
  end

  def handle_call(:zoom, _from, %{path: path} = state) do
    :dets.insert(:frame_table, {path, true})

    update_bg(path, true)

    {:reply, {:ok, path}, state}
  end

  def handle_call(:zoom_out, _from, %{path: path} = state) do
    :dets.insert(:frame_table, {path, false})

    update_bg(path, false)

    {:reply, {:ok, path}, state}
  end

  defp update_bg(path, zoom \\ false) do
    bg = Application.fetch_env!(:frame, :bg)

    if bg == true and System.get_env("DISPLAY") != nil do
      if zoom do
        System.cmd("feh", ["--auto-rotate", "--bg-fill", path])
      else
        System.cmd("feh", ["--auto-rotate", "--bg-max", path])
      end
    end
  end
end

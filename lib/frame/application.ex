defmodule Frame.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      FrameWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Frame.PubSub},
      # Start the Endpoint (http/https)
      FrameWeb.Endpoint,
      # Start a worker by calling: Frame.Worker.start_link(arg)
      # {Frame.Worker, arg}
      {Frame.Album, Application.fetch_env!(:frame, :root)},
      {Frame.Timer, Application.fetch_env!(:frame, :time)}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Frame.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    FrameWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

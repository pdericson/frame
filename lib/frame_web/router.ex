defmodule FrameWeb.Router do
  use FrameWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {FrameWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FrameWeb do
    pipe_through :browser

    live "/", PageLive, :index
  end

  # Other scopes may use custom stacks.
  scope "/api/v1", FrameWeb do
    pipe_through :api

    get "/frame", FrameController, :show
    get "/album", AlbumController, :show
    post "/album/prev", AlbumController, :prev
    post "/album/next", AlbumController, :next
    post "/album/zoom", AlbumController, :zoom
    post "/album/zoom_out", AlbumController, :zoom_out
  end

  scope "/api/swagger" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI, otp_app: :frame, swagger_file: "swagger.json"
  end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Frame",
        description: "This is the API for my Photo Frame project for the Raspberry Pi.",
        contact: %{
          name: "Peter Ericson",
          email: "pdericson@pdericson.com",
          url: "https://gitlab.com/pdericson/frame"
        }
      }
    }
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: FrameWeb.Telemetry
    end
  end
end

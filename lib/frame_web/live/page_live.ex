defmodule FrameWeb.PageLive do
  use FrameWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket), do: :timer.send_interval(1000, self(), :tick)

    path = update()

    {:ok, assign(socket, path: path)}
  end

  @impl true
  def handle_info(:tick, socket) do
    path = update()

    {:noreply, assign(socket, path: path)}
  end

  defp update() do
    {:ok, path} = Frame.Album.path()

    root = Application.fetch_env!(:frame, :root)

    path |> String.replace_prefix(root, "/frame")
  end
end

defmodule FrameWeb.FrameController do
  use FrameWeb, :controller
  use PhoenixSwagger

  def swagger_definitions do
    %{
      Frame:
        swagger_schema do
          title("Frame")

          properties do
            root(:string, "The root photos directory.", required: true)
            time(:integer, "The time (in seconds) that each photo is shown.", required: true)
          end

          example(%{
            root: "/mnt",
            time: 600
          })
        end
    }
  end

  swagger_path :show do
    get("/api/v1/frame")
    description("Returns the frame configuration.")
    response(200, "OK", Schema.ref(:Frame))
  end

  def show(conn, _params) do
    root = Application.fetch_env!(:frame, :root)
    time = Application.fetch_env!(:frame, :time)

    json(conn, %{root: root, time: time})
  end
end

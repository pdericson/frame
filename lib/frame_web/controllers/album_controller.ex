defmodule FrameWeb.AlbumController do
  use FrameWeb, :controller
  use PhoenixSwagger

  def swagger_definitions do
    %{
      Album:
        swagger_schema do
          title("Album")

          properties do
            path(:string, "The path to the current photo.", required: true)
          end

          example(%{
            path: "/mnt/jumbo1.jpg"
          })
        end
    }
  end

  swagger_path :show do
    get("/api/v1/album")
    description("Returns the album configuration.")
    response(200, "OK", Schema.ref(:Album))
  end

  def show(conn, _params) do
    {:ok, path} = Frame.Album.path()

    json(conn, %{path: path})
  end

  swagger_path :prev do
    post("/api/v1/album/prev")
    description("Returns the prev photo.")
    response(200, "OK", Schema.ref(:Album))
  end

  def prev(conn, _params) do
    {:ok, path} = Frame.Album.prev()

    json(conn, %{path: path})
  end

  swagger_path :next do
    post("/api/v1/album/next")
    description("Returns the next photo.")
    response(200, "OK", Schema.ref(:Album))
  end

  def next(conn, _params) do
    {:ok, path} = Frame.Album.next()

    json(conn, %{path: path})
  end

  swagger_path :zoom do
    post("/api/v1/album/zoom")
    description("Zoom.")
    response(200, "OK", Schema.ref(:Album))
  end

  def zoom(conn, _params) do
    {:ok, path} = Frame.Album.zoom()

    json(conn, %{path: path})
  end

  swagger_path :zoom_out do
    post("/api/v1/album/zoom_out")
    description("Zoom out.")
    response(200, "OK", Schema.ref(:Album))
  end

  def zoom_out(conn, _params) do
    {:ok, path} = Frame.Album.zoom_out()

    json(conn, %{path: path})
  end
end
